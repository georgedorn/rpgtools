from django.shortcuts import render, redirect
from party.models.combat import Combat, Combatant
from django.urls.base import reverse


def show_combat(request, combat_id=None):
    if combat_id:
        combat = Combat.objects.get(pk=combat_id)
    else:
        combat = Combat.objects.latest()
        canonical_url = reverse('player_combat_view_by_id', kwargs={'combat_id': combat.pk})
        return redirect(canonical_url)

    context = {}

    context['combatants'] = [Combatant.objects.get(pk=pk) for pk in combat.positions]
    context['current_combatant'] = combat.current_turn

    context['show_secrets'] = bool('sk' in request.GET and request.GET.get('sk') == combat.game.secret_key)

    return render(request, 'combat.html', context)