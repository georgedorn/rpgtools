# urls.py
from django.urls import path, include
from party.api import CombatantResource, CombatResource
from tastypie.api import Api


api = Api("combat")
api.register(CombatResource())
# api.register(TableResource())

urlpatterns = [
    path("", include(api.urls)),
]
