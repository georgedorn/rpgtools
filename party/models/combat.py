from collections import defaultdict
from random import shuffle

from django.db import models

from .base import TimeStampModel
from django.db.models.query_utils import Q
import re
from django.urls.base import reverse_lazy
from django.utils.http import urlencode


class RemovingCurrentTurnException(RuntimeError):
    pass


class Combatant(TimeStampModel):
    """
    Through table to Character, with meta-data about things like current initiative, overridden monster
    names ("goblin #7").
    Possibly: combat effects, flat-footed state, etc.

    Initiative tracking:
    - init is rolled when creature is added to combat
    - once everybody has an init, position is determined:
    -- Sort by rolled init.
    -- Resolve ties by comparing init bonus
    -- Resolve remaining ties randomly.
    """

    creature = models.ForeignKey(
        "Creature",
        related_name="characters",
        on_delete=models.CASCADE,
    )
    combat = models.ForeignKey(
        "Combat",
        related_name="combatants",
        on_delete=models.CASCADE,
    )
    name = models.CharField(max_length=256, blank=False, null=False)
    initiative = models.IntegerField(
        null=True, help_text="Stores current initiative roll"
    )
    is_dead = models.BooleanField(
        default=False,
        help_text="Indicates this combatant is dead or otherwise incapacitated",
    )
    is_hidden = models.BooleanField(
        default=False,
        help_text="Indicates this combatant is hidden from the party and should not appear in player views",
    )
    is_ready = models.BooleanField(
        default=False, blank=True, help_text="Indicates character holding an action"
    )
    ready_reason = models.TextField(
        default=None,
        null=True,
        blank=True,
        help_text="Optional reason for holding an action, for display purposes",
    )

    def __str__(self):
        return "{NAME} ({INIT})".format(
            NAME=self.name or self.creature.name,
            INIT=self.initiative,
        )

    def _update_field(self, field_name, new_value):
        setattr(self, field_name, new_value)
        self.save(update_fields=(field_name,))

    def mark_hidden(self):
        self._update_field("is_hidden", True)

    def mark_visible(self):
        self._update_field("is_hidden", False)
        self.combat.renumber_combatant(
            self
        )  # Hidden things aren't numbered until last minute

    def mark_dead(self):
        self._update_field("is_dead", True)

    def mark_alive(self):
        self._update_field("is_dead", False)

    def roll_initiative(self):
        self.initiative = self.creature.roll_initiative()
        self.save(update_fields=("initiative",))
        return self.initiative


# Need a way to persist combat state.  Maybe just serialize everything?
class Combat(TimeStampModel):
    game = models.ForeignKey(
        "Game",
        on_delete=models.CASCADE,
    )
    participants = models.ManyToManyField("Creature", through="Combatant")

    positions = models.JSONField(null=True, blank=True)
    current_turn = models.ForeignKey(
        "Combatant",
        default=None,
        null=True,
        related_name="+",
        help_text="Combatant whose turn it is.",
        on_delete=models.SET_NULL,
    )
    current_round = models.PositiveIntegerField(
        default=0, help_text="Tracks the number of rounds passed in the combat"
    )

    class Meta:
        get_latest_by = "pk"

    def get_api_detail_url(self):
        """
        Returns the public detail API endpoint for this combat.
        """
        return reverse_lazy(
            "api_dispatch_detail",
            kwargs={"resource_name": "combats", "api_name": "combat", "pk": self.pk},
        )

    def get_public_url(self):
        """
        The public web UI to the combat.
        """
        return reverse_lazy("player_combat_view_by_id", kwargs={"combat_id": self.pk})

    def get_dm_url(self):
        """
        The url to the secret, DM-only view.  This automatically calls set_secret_key() if it is currently
        None.
        """
        if self.game.secret_key is None:
            self.game.set_secret_key()
        public_url = self.get_public_url()
        return public_url + "?" + urlencode({"sk": self.game.secret_key})

    def get_combatants_by_name(self, name, type=None):  # noqa
        """
        Returns multiple combatants whose creature name matches 'name'.  Optional 'type' allowed.
        """
        return self.get_combatants_by_filters(name=name, type=type)

    def get_combatants_by_filters(self, **kwargs):
        """
        Helper method to get all Combatant objects matching certain filters,
        like name or type.

        TODO: Add more filters?  Use Django-filters?  Pass-through arbitrary other parameters?
        """
        combatants = self.combatants.all()
        name = kwargs.pop("name", None)
        if name is not None:
            # if the name is "monster #8", user is trying to find a specific combatant, not all
            # monsters with that name
            combatant_name_regex = "\#\d+$"  # string ends with # and at least one digit
            if re.search(combatant_name_regex, name):
                combatants = combatants.filter(name__iexact=name)
            else:
                combatants = combatants.filter(creature__name__iexact=name)
        creature_type = kwargs.pop("type", None)
        if creature_type is not None:
            combatants = combatants.filter(creature__type=creature_type)
        if kwargs:
            # pass any arbitrary kwargs along
            combatants = combatants.filter(**kwargs)
        return combatants

    def get_one_combatant_by_filters(self, **kwargs):
        combatants = self.get_combatants_by_filters(**kwargs)
        try:
            return combatants.get()
        except Combatant.MultipleObjectsReturned:
            count = combatants.count()
            raise Combatant.MultipleObjectsReturned(
                "Filters not specific, matched %s combatants" % count
            )

    def _mark_one(self, field_name, new_value, **kwargs):
        """
        Marks one combatant specified in kwargs.
        """
        combatant = self.get_one_combatant_by_filters(**kwargs)
        setattr(combatant, field_name, new_value)
        combatant.save(update_fields=(field_name,))
        return combatant

    def _mark_all(self, field_name, new_value, **kwargs):
        """
        Helper method to set a field on all combatants matching the filters in kwargs.
        """
        combatants = self.get_combatants_by_filters(**kwargs)
        return combatants.update(**{field_name: new_value})

    def mark_hidden(self, name=None, type=None, **kwargs):  # noqa
        return self._mark_one("is_hidden", True, name=name, type=type, **kwargs)

    def mark_visible(self, name=None, type=None, **kwargs):  # noqa
        combatant = self.get_one_combatant_by_filters(name=name, type=type, **kwargs)
        combatant.is_hidden = False
        combatant.save()
        self.renumber_combatant(combatant)

    def mark_dead(self, name=None, type=None, **kwargs):  # noqa
        return self._mark_one("is_dead", True, name=name, type=type, **kwargs)

    def mark_alive(self, name=None, type=None, **kwargs):  # noqa
        return self._mark_one("is_dead", False, name=name, type=type, **kwargs)

    def mark_all_hidden(self, name=None, type=None, **kwargs):  # noqa
        return self._mark_all("is_hidden", True, name=name, type=type, **kwargs)

    def mark_all_visible(self, name=None, type=None, **kwargs):  # noqa
        combatants = self.get_combatants_by_filters(name=name, type=type, **kwargs)
        res = combatants.update(is_hidden=False)
        for combatant in combatants:
            self.renumber_combatant(combatant)
        return res

    def mark_all_dead(self, name=None, type=None, **kwargs):  # noqa
        return self._mark_all("is_dead", True, name=name, type=type, **kwargs)

    def mark_all_alive(self, name=None, type=None, **kwargs):  # noqa
        return self._mark_all("is_dead", False, name=name, type=type, **kwargs)

    def remove(self, name=None, type=None, **kwargs):  # noqa
        """
        Removes a combatant by name.  This isn't a great API, it takes a string where
        add() takes a bunch of things.
        """
        combatant = self.get_one_combatant_by_filters(name=name, type=type, **kwargs)
        if self.current_turn == combatant:
            raise RemovingCurrentTurnException(
                "Cannot remove current combatant; advance turn first"
            )
        if self.positions:
            self.positions.remove(combatant.pk)
            self.save(update_fields=("positions",))
        return combatant.delete()

    def remove_all(self, name=None, type=None, **kwargs):  # noqa
        """
        Removes all combatants matching name/type/etc.
        This isn't a great API, it takes a string where add() takes a bunch of things.
        """
        combatants = self.get_combatants_by_filters(name=name, type=type, **kwargs)
        for combatant in combatants:
            if self.current_turn == combatant:
                raise RemovingCurrentTurnException(
                    "Cannot remove current combatant; advance turn first"
                )
            if self.positions:
                self.positions.remove(combatant.pk)
        deletes = combatants.delete()
        self.save(update_fields=("positions",))
        return deletes

    @property
    def started(self):
        """
        Determines if combat has already started; if so, adding a new monster rolls their initiative
        immediately and adds them to the combat.
        """
        return bool(self.positions)

    def get_combatants_by_position(self, with_hidden=False):
        if not self.positions:
            raise RuntimeError("Must run initialize() to populate positions")
        combatants = self.combatants.all()
        if not with_hidden:
            combatants = combatants.filter(is_hidden=False)

        combatants_by_pk = {c.pk: c for c in combatants}

        combatants_by_position = [
            combatants_by_pk[pk] for pk in self.positions if pk in combatants_by_pk
        ]
        return combatants_by_position

    @property
    def combatants_by_position(self):
        """
        Returns a list of combatants, sorted by 'position'.
        """
        return self.get_combatants_by_position(with_hidden=False)

    @property
    def all_combatants_by_position(self):
        """
        Returns a list of all combatants, including hidden combatants, sorted by position.
        """
        return self.get_combatants_by_position(with_hidden=True)

    @property
    def next_turn(self):
        """
        Returns the Combatant object for the next Combatant's turn.
        """
        current_turn_index = self.positions.index(self.current_turn.pk)
        # TODO: Off-by-one edge case below, make sure modulo is correct here
        next_turn_index = (current_turn_index + 1) % len(self.positions)

        next_turn_pk = self.positions[next_turn_index]
        return Combatant.objects.get(pk=next_turn_pk)

    def clear_all(self):
        """
        Removes all combatants from a combat.
        """
        self.combatants.all().delete()
        updated_fields = []
        if self.positions:
            self.positions = []
            updated_fields.append("positions")
        if self.current_round:
            self.current_round = 0
            updated_fields.append("current_round")

        if updated_fields:
            self.save(update_fields=updated_fields)

    def clear(self, party=None):
        """
        Ends a combat.
        Removes all non-party-members and resets the round counter to 0.
        """
        self.refresh_from_db()
        updated_fields = []
        if self.positions:
            self.positions = []
            updated_fields.append("positions")
        if self.current_round:
            self.current_round = 0
            updated_fields.append("current_round")
        if self.current_turn:
            self.current_turn = None
            updated_fields.append("current_turn")
        if updated_fields:
            self.save(update_fields=updated_fields)

        # If a 'party' object is passed in, remove everything not belonging to the party
        if party is not None:
            for combatant in self.combatants.all():
                if combatant.creature.party != party:
                    combatant.delete()
        # Otherwise, remove only monsters
        else:
            for combatant in self.combatants.all():
                if combatant.creature.type == "monster":
                    combatant.delete()

    def stop(self):
        """
        Ends a combat, retaining all combatants but clearing their initiatives and positions.
        """
        self.positions = []
        self.current_turn = None
        self.save(update_fields=("positions", "current_turn"))
        for combatant in self.combatants.all():
            combatant.initiative = None
            combatant.save(update_fields=("initiative",))

    def get_combatant_by_name(self, name: str, type=None) -> Combatant:  # noqa
        """
        Finds a combatant in this combat using their name.
        Raises an exception in the case of ambiguity.
        """
        queryset = Combatant.objects.filter(Q(name=name) | Q(creature__name=name))
        if type is not None:
            queryset = queryset.filter(creature__type=type)
        try:
            return queryset.filter(combat=self).get()
        except Combatant.MultipleObjectsReturned:
            raise RuntimeError("Ambiguous creature; multiple creatures have that name.")

    def add(self, creature, type=None, count=1, add_late=True):  # noqa
        """
        Add a creature to the combat.  `creature` can be a Creature instance or a string;
        if a string, `type` is required.
        `count` adds that many creatures of the same type, recursively
        `add_late` - if combat is already started, `True` automatically delays the combatant to the current
            init count, if it rolls higher than it.
        """
        from party.models.party import Creature

        if not isinstance(creature, Creature):
            if type is None:
                raise ValueError(
                    "Cannot add a creature by name without specifying type"
                )

            creature = Creature.objects.get(game=self.game, type=type)

        if count > 1:
            return [self.add(creature, type=type, count=1) for _ in range(count)]

        if creature.type in ("PC", "NPC"):
            # unique creature types can only be added once
            if Combatant.objects.filter(creature=creature, combat=self).exists():
                raise ValueError(
                    "Unique creature %s is already in this combat" % creature.name
                )

        combatant = Combatant.objects.create(
            combat=self, creature=creature, name=creature.name
        )
        if self.started:
            self.add_late_combatant(combatant, move_before=add_late)

        return combatant

    def add_late_combatant(self, combatant, move_before=True):
        """
        Sorts a newcomer into the initiative order for a combat that's already started.

        Renumbers the newcomer if it is a duplicate.
        """
        if not self.started:
            raise ValueError("Combat not started, use combat.start()")

        initiative = combatant.roll_initiative()

        # degenerate case:  initiative is higher than the current turn
        # in this case, the creature can go immediately (or could delay)
        # so just move them before the current turn
        if move_before and initiative > self.current_turn.initiative:
            return self.move_combatant_before(combatant, self.current_turn)

        # Otherwise, re-run the sort logic but don't modify current_turn,
        # except in rare edge case that new combatant's init ties current_turn's init
        self.positions = [c.pk for c in self.sort_combatants()]

        if move_before and self._is_before(combatant, self.current_turn):
            self.current_turn = combatant

        self.save()

        self.renumber_combatant(combatant)

        return self.current_turn

    def renumber_combatant(self, combatant):
        """
        Numbers a single combatant, either added late to the combat or just becoming visible to the PCs.
        """
        if combatant.is_hidden:
            return False  # don't number hidden things, it can leave a gap that reveals their existence
        if combatant.name != combatant.creature.name:
            return False  # combatant has probably already been renumbered

        dupe_combatants = self.combatants.filter(creature=combatant.creature)
        if dupe_combatants.count() <= 1:
            return False  # only 1 or less creatures of this type in the fight, no need to number

        dupe_combatant_names = dupe_combatants.values_list("name", flat=True)

        i = 1
        possible_name = "{NAME} #{NUMBER}".format(
            NAME=combatant.creature.name, NUMBER=i
        )
        while possible_name in dupe_combatant_names:
            i += 1
            possible_name = "{NAME} #{NUMBER}".format(
                NAME=combatant.creature.name, NUMBER=i
            )

        combatant.name = possible_name
        combatant.save()

    def _is_before(self, combatant, target):
        """
        Tests whether combatant's turn is before target's in initiative order.
        """
        return self.positions.index(combatant.pk) < self.positions.index(target.pk)

    def move_combatant_before(
        self, combatant, target, combatant_type=None, target_type=None
    ):
        """
        Move `combatant` to before `target` in the initiative order.
        This changes the combatant's init roll to match target's.

        If it was currently the target's turn, it becomes the combatant's turn;
        this is to support ready actions.
        """
        if isinstance(combatant, str):
            combatant = self.get_combatant_by_name(name=combatant, type=combatant_type)
        if isinstance(target, str):
            target = self.get_combatant_by_name(name=target, type=target_type)

        if combatant == target:
            return False  # can't move in front of yourself
        elif self.current_turn == combatant:
            # it's currently the moving combatant's turn, so we should advance the turn
            self.advance_turn()

        combatant.initiative = target.initiative
        if combatant.pk in self.positions:
            self.positions.remove(combatant.pk)
        target_index = self.positions.index(target.pk)
        self.positions.insert(target_index, combatant.pk)

        if self.current_turn == target:
            self.current_turn = combatant

        # TODO: Use a lock to avoid race conditions
        combatant.save()
        self.save()
        return self.current_turn

    def move_combatant_after(
        self, combatant, target, combatant_type=None, target_type=None
    ):
        """
        Moves a combatant to immediately after the target in the turn order.  Useful for delaying until
        just after another PC.
        """
        if isinstance(combatant, str):
            combatant = self.get_combatant_by_name(name=combatant, type=combatant_type)
        if isinstance(target, str):
            target = self.get_combatant_by_name(name=target, type=target_type)

        if combatant == target:
            return False  # can't move in front of yourself
        elif self.current_turn == combatant:
            # it's currently the moving combatant's turn, so we should advance the turn
            self.advance_turn()

        combatant.initiative = target.initiative
        if combatant.pk in self.positions:
            self.positions.remove(combatant.pk)
        target_index = self.positions.index(target.pk) + 1  # after
        self.positions.insert(target_index, combatant.pk)

        combatant.save()
        self.save()
        return self.current_turn

    def initialize(self):
        """
        Used during the start of a combat to:
        - number duplicate monsters ('goblin #2')
        - sort combatants by initiative and save them in self.positions
        - determine who goes first
        - sets round counter to 0
        """
        self.fix_monster_names()
        sorted_combatants = list(self.sort_combatants())
        self.positions = [c.pk for c in sorted_combatants]
        if sorted_combatants:
            self.current_turn = sorted_combatants[0]
        else:
            self.current_turn = None
        self.current_round = 0
        self.save(update_fields=("positions", "current_turn", "current_round"))

    def start(self):
        """
        Starts or restarts combat, rolling initiative for everybody involved and calling initialize().
        """
        self.positions = []
        self.save()
        for combatant in self.combatants.all():
            combatant.roll_initiative()
        self.initialize()
        return self.current_turn

    def advance_turn(self):
        """
        Moves the current turn pointer to the next combatant in turn order.
        If this starts over at the top of the round, updates the current_round counter.
        TODO: Use a lock to avoid race conditions.
        """
        updated_fields = ["current_turn"]
        old_combatant = self.current_turn
        old_pk = old_combatant.pk
        index = self.positions.index(old_pk)
        new_index = (index + 1) % len(self.positions)
        new_pk = self.positions[new_index]
        self.current_turn = Combatant.objects.get(pk=new_pk, combat=self)

        if new_index == 0:
            self.current_round += 1
            updated_fields.append("current_round")

        self.save(update_fields=updated_fields)

        return self.current_turn

    def fix_monster_names(self):
        """
        Go through combat, renaming monsters with #1, #2, etc, if they have the same name.
        TODO: This should be idempotent if combat unchanged, and only add numbers to newly-added creatures.
        """
        monster_bin = self._monsters_by_name
        for monster_name in monster_bin:
            if len(monster_bin[monster_name]) > 1:
                self.renumber_monsters(monster_bin[monster_name])

    def renumber_monsters(self, monster_list):
        """
        Given a list of Combatants, number each of them.
        """
        existing_names = set(self.combatant_names)
        #        existing_numbers = set()
        #        for name in existing_names:
        #            parts = name.split('#')
        #            try:
        #                existing_numbers.add(int(parts[-1]))
        #            except (ValueError, IndexError):
        #                pass  # not a number, or not numbered at all

        for number, combatant in enumerate(monster_list):
            if combatant.name in existing_names:
                existing_names.remove(combatant.name)
            name = "{NAME} #{NUMBER}".format(
                NAME=combatant.creature.name, NUMBER=number + 1
            )  # +1 because enumerate starts at 0
            while name in existing_names:
                number += 1
                name = "{NAME} #{NUMBER}".format(
                    NAME=combatant.creature.name, NUMBER=number + 1
                )  # +1 because enumerate starts at 0

            combatant.name = name
            combatant.save(update_fields=["name"])
            existing_names.add(combatant.name)

    @property
    def _monsters_by_name(self):
        """
        Returns a dict of 'monster_name': [list of Combatants] for each name of monster in the combat.
        """
        monster_bin = defaultdict(list)
        for combatant in self.combatants.filter(
            creature__type="monster", is_hidden=False
        ):
            monster_bin[combatant.creature.name].append(combatant)
        return monster_bin

    @property
    def combatant_names(self):
        """
        Returns a set of all of the names of combatants (including numbering).
        """
        return set([combatant.name for combatant in self.combatants.all()])

    def sort_combatants(self):
        """
        Establishes initial turn order, rolling initiative for all participants and sorting them
        according to 3.5 rules.
        TODO: support sorting by other algorithms somehow?
        """
        combatants = defaultdict(list)
        for combatant in self.combatants.all():
            if combatant.initiative is None:
                combatant.roll_initiative()
            combatants[combatant.initiative].append(combatant)
        # combatants.sort(reverse=True)  # TODO:  suport low-to-high inits?
        for key in sorted(combatants.keys(), reverse=True):
            if len(combatants[key]) == 1:
                yield combatants[key][0]
            else:
                ties = defaultdict(list)
                for combatant in combatants[key]:
                    ties[combatant.creature.get_stat("init")].append(combatant)
                for key in sorted(ties.keys(), reverse=True):
                    if len(ties[key]) == 1:
                        yield ties[key][0]
                    else:
                        # note this is non-deterministic; don't call this more than once or these could change
                        shuffle(ties[key])
                        for combatant in ties[key]:
                            yield combatant


# class Initiative(models.Model):
#    """
#    Through table, associating a character to a combat, storing per-combat statistics like
#    initiative and temporary effects.
#    """
#    creature = models.ForeignKey(Creature)
#    combat = models.ForeignKey("Combat")

# Initiative is a float to handle ties gracefully, and to allow PCs to delay or ready actions.
#    initiative = models.FloatField(null=True, default=None, help_text="Sort order for character")
#    effects = JSONField(default=list, help_text='List of strings describing effects.')
#    creature_name = models.CharField(
#        max_length=259, null=True, blank=True,
#        help_text="When associated character is a Monster, the name of the individual monster "
#                  "e.g. 'goblin #2'")

# class Combat(models.Model):
#    """
#    Stores a collection of combat participants
#    """
#    game = models.ForeignKey(Game)
#    creatures = models.ManyToManyField(to=Creature, through=Initiative)
#    current_initiative = models.ForeignKey(Initiative, null=True, related_name='+',
#        help_text="Holds current position in turn order.")

#    def add_creature(self, creature, number=1):
#        """
#        Adds a creature object to the combat.  If this is a monster, duplicates will be handled by setting
#        per-initiative names (e.g. goblin #2)
#        """
#        for _ in range(number):
#            Initiative.objects.create(creature=creature,
#                                      combat=self,
#                                      creature_name=creature.name,
#                                      initiative=creature.roll('initiative'),
#                                      effects=creature.base_effects,
#                                      )
#
#    def get_creatures_by_init(self):
#        res = [(c.initiative, c)
#               for c in self.creatures
#               ]
#        res.sort(reverse=True)
#        return res
#
#    def resolve_ties(self):
#        """
#        Call when multiple creatures have been added to combat; it's preferable to wait until
#        everybody is added, though late additions can also use this for resolution.
#        """
#        creatures_by_init = self.get_creatures_by_init()
# how to?

# for each integer, if there's more than one participant with that roll,
# 1. add the participant's dex modifier / 100.
# 2. if there is still a tie, randomly add .0001 to one of them.
# 3. repeat #2 until no more ties for that group


# alt:
# store a tie-resolution number for sub-sorting participants
# requires resetting
