from django.db import models
import ast


class TimeStampModel(models.Model):
    """
    Mixin to add created_at / updated_at fields.
    """

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
