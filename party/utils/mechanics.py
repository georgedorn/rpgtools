"""
Utilities for implementing game mechanics, like rolling dice.
"""
import random
import re

def roll_dice(number, sides, bonus=0):
    return sum([random.randint(1, sides)
                for _ in range(number)]) + int(bonus)

def digest_stat_name(stat_name):
    """
    Returns a very simplified version of stat_name, for matching another similarly-digested stat name
    against.
    
    E.g.s:
    - 'Initiative' becomes 'initiative'
    - 'Knowledge (Nature)' becomes 'knowledge nature'
    - 'Knowledge - Arcane' becomes 'knowledge arcane'
    """
    new_stat_name = stat_name.lower().strip()
    non_alphanum = re.compile('[^a-z0-9 ]')
    new_stat_name = non_alphanum.sub('', new_stat_name)
    return re.sub("\s\s+", ' ', new_stat_name)
