from django.test import TestCase

from party.utils.mechanics import digest_stat_name


class TestStatName(TestCase):

    def test_lowercase(self):
        self.assertEqual(digest_stat_name('Initiative'), 'initiative')
        self.assertEqual(digest_stat_name('initiative'), 'initiative')

    def test_strip_punctuation(self):
        self.assertEqual(digest_stat_name('Knowledge (Nature)'),
                         'knowledge nature')
        self.assertEqual(digest_stat_name('Knowledge: Nature'),
                         'knowledge nature')
        self.assertEqual(digest_stat_name('Knowledge - Nature'),
                         'knowledge nature')

    def test_strip_extra_spaces(self):
        self.assertEqual(digest_stat_name('Craft  Weaponsmithing'),
                         'craft weaponsmithing')

    def test_strip_surrounding_spaces(self):
        self.assertEqual(digest_stat_name(' Initiative'), 'initiative')
        self.assertEqual(digest_stat_name('Initiative '), 'initiative')
        self.assertEqual(digest_stat_name(' Initiative '), 'initiative')

        
        