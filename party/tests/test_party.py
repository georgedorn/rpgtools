from django.test import TestCase
from ..models import Game, Creature, Party


class PartyModelsTestCase(TestCase):
    """
    Tests of fat model behaviors.
    """
    def setUp(self):
        self.game = Game.objects.create(name='test game')
        self.pc = Creature.objects.create(type='PC', name='Bob the Paladin',
                                          stats={'Spot': 3,
                                                 'Craft (Weaponsmithing)': 2,
                                                 'Hide': -1,
                                                 'Jump': 1})
        self.party_foo = Party.objects.create(name='foo', game=self.game)
        self.party_bar = Party.objects.create(name='bar', game=self.game)

    def test_level_up(self):
        self.pc.level = 5
        self.pc.stats = {'Spot': 3, 'Hide': -1}
        self.pc.previous_stats = {'Spot': 2, 'Hide': -1, 'Jump': 1}
        self.pc.save()
        self.pc.level_up()

        self.assertEqual(self.pc.level, 6)
        self.assertEqual(self.pc.stats, {})
        self.assertEqual(self.pc.get_stat('Spot'), 3)
        self.assertEqual(self.pc.get_stat('Hide'), -1)
        self.assertEqual(self.pc.get_stat('Jump'), 1)

    def test_party_load_by_name(self):
        self.assertEqual(Party.load('bar'), self.party_bar)
        self.assertEqual(Party.load('foo'), self.party_foo)

    def test_party_load_by_latest(self):
        self.assertEqual(Party.load(), self.party_bar)  # last updated
        Party.load('foo')  # explicitly load foo
        self.assertEqual(Party.load(), self.party_foo)  # last loaded

    def test_get_stat_insensitive(self):
        self.assertEqual(self.pc.get_stat('spot'), 3)
        self.assertEqual(self.pc.get_stat('Spot'), 3)
        self.assertEqual(self.pc.get_stat('spOt'), 3)
        self.assertEqual(self.pc.get_stat('SPOT'), 3)

    def test_get_stat_punctuation(self):
        self.assertEqual(self.pc.get_stat('Craft (Weaponsmithing)'), 2)
        self.assertEqual(self.pc.get_stat('Craft Weaponsmithing'), 2)
        self.assertEqual(self.pc.get_stat('Craft: Weaponsmithing'), 2)
        self.assertEqual(self.pc.get_stat('craft weaponsmithing'), 2)

    def test_get_previous_stat_digested(self):
        """
        Fallback to prior stats also uses digests.
        """
        self.pc.level_up()

        # should have no stats, only previous stats
        self.assertEqual(self.pc.stats, {})
        self.assertNotEqual(self.pc.previous_stats, {})

        self.assertEqual(self.pc.get_stat('Craft (Weaponsmithing)'), 2)
        self.assertEqual(self.pc.get_stat('Craft Weaponsmithing'), 2)
        self.assertEqual(self.pc.get_stat('Craft: Weaponsmithing'), 2)
        self.assertEqual(self.pc.get_stat('craft weaponsmithing'), 2)

