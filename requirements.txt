Django<4.2
django-tastypie==0.14.5  # likely locked to specific Django versions
django-autoslug~=1.9.8
ipython<6.3  # watch this, ipython's API shifts a lot.
pyparsing<=3.1
django-extensions<=3.3
-e git+https://github.com/georgedorn/diceroll.git#egg=diceroll  # TODO: Merge this

beautifulsoup4<5.0

coverage<=7.2

