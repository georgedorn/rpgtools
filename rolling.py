from collections import Counter
from party.utils.mechanics import roll_dice

TOTAL_ROLLS = 10000

def calc_duration(sides=20):
  total = 1
  while roll_dice(number=1, sides=sides) > 1:
    total += 1
    sides = max(sides-2, 2)
  return total



for sides in (20, 12, 10, 8, 6, 4, 2):
  print("Sides:", sides)
  counts = Counter()
  for i in range(TOTAL_ROLLS):
    counts[calc_duration(sides=sides)] += 1
  for duration, total in sorted(counts.items()):
    print("Duration: {}, Occurance: {}%".format(duration, float(total)*100 / TOTAL_ROLLS))
