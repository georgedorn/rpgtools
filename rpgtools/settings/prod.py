from .base import *  # noqa

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'rpgtools',
        'USER': 'rpgtools',
        'PASSWORD': os.environ['DJANGO_POSTGRESQL_PASSWORD'],
#        'HOST': '',
#        'PORT': 'db_port_number',
    }
}
