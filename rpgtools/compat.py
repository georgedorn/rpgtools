from django.conf import settings


if "postgres" in settings.DATABASES["default"]["ENGINE"]:
    from django.contrib.postgres.fields.jsonb import JSONField
else:
    from django.db.models.fields.json import JSONField
