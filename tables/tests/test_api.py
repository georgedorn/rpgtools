from django.test import TestCase
from tables.models import Table, TableEntry
from django.contrib.auth.models import User
from django.urls.base import reverse_lazy, reverse


class TableAPITestcase(TestCase):

    @classmethod
    def setUpTestData(cls):
        super(TableAPITestcase, cls).setUpTestData()
        cls.user = User(username='test_user')
        cls.user.set_password('test_pass')
        cls.user.save()

        cls.parent_table = Table.objects.create(name='parent_table', owner=cls.user)
        cls.child_table = Table.objects.create(name='child_table', parent=cls.parent_table, owner=cls.user)

        for i in range(10):
            TableEntry.objects.create(text='Entry #{i}'.format(i=i), weight=i, table=cls.parent_table)
            TableEntry.objects.create(text='Entry #{i}'.format(i=i), weight=i, table=cls.child_table)

        cls.tables_url = reverse('api_dispatch_list', kwargs={'resource_name': 'tables', 'api_name':'v1'})

    def test_get_tables(self):
        res = self.client.get(self.tables_url)
        objects = res.json()['objects']
        self.assertEqual(len(objects), 2)
        self.assertEqual(objects[0]['name'], self.parent_table.name)
        self.assertEqual(objects[1]['name'], self.child_table.name)

    def test_get_table_entries_nested_url(self):
        """
        Using the /api/v1/tables/{table}/table_entries endpoint, we should only get back
        entries belonging to {table}.
        """
        top_table_entries_url = reverse('api_dispatch_list', kwargs={'resource_name': 'table_entries',
                                                                     'api_name': 'v1',
                                                                     'table': self.parent_table.slug})
        res = self.client.get(top_table_entries_url)
        objects = res.json()['objects']
        expected_table_url = reverse('api_dispatch_detail', kwargs={'resource_name': 'tables',
                                                                    'api_name': 'v1',
                                                                    'pk': self.parent_table.pk})
        for obj in objects:
            self.assertEqual(obj['table'], expected_table_url)
