from django.test import TestCase, tag

from tables.models import Table, TableEntry
from tables.parse_utils import get_table_by_dotted_name, TableNotFound
from django.db.utils import IntegrityError


class TestTables(TestCase):

    @classmethod
    def setUpTestData(cls):
        super(TestTables, cls).setUpTestData()
        cls.table = Table.objects.create(name='test table')
        cls.entry1 = TableEntry.objects.create(table=cls.table,
                                           text='Entry 1',
                                           weight=25)
        cls.entry2 = TableEntry.objects.create(table=cls.table,
                                           text='Entry 2',
                                           weight=75)
        cls.parent_table = Table.objects.create(name='parent')
        cls.child_table = Table.objects.create(name='child', parent=cls.parent_table)
        cls.grandchild_table = Table.objects.create(name='grandchild', parent=cls.child_table)

    def test_table_weights(self):
        self.assertEqual(self.table.total_weight, 100)

    def test_table_entries_dict(self):
        entries_by_weight = self.table.entries_by_weight
        self.assertEqual(len(entries_by_weight), self.table.total_weight)

    def test_table_get_random_entry(self):
        entry = self.table.get_random_entry()
        self.assertTrue(entry in (self.entry1, self.entry2))

    @tag('slow')
    def test_get_random_entry_probabilities(self):
        """
        If we pick a thousand entries from the table, they should be roughly
        proportionate to the original weights.
        """
        entry1_count = 0
        entry2_count = 0
        for _ in range(1000):
            entry = self.table.get_random_entry()
            if entry == self.entry1:
                entry1_count += 1
            elif entry == self.entry2:
                entry2_count += 1
            else:
                raise Exception("Picked a non-existant entry")
        ratio = float(entry2_count) / float(entry1_count)
        self.assertTrue(ratio > 2.0)
        self.assertTrue(ratio < 4.0)

    @tag('slow')
    def test_get_random_entry_db_probabilities(self):
        """
        If we pick a thousand entries from the table, they should be roughly
        proportionate to the original weights.
        """
        entry1_count = 0
        entry2_count = 0
        for _ in range(1000):
            entry = self.table.get_random_entry_db()
            if entry == self.entry1:
                entry1_count += 1
            elif entry == self.entry2:
                entry2_count += 1
            else:
                raise Exception("Picked a non-existant entry")
        ratio = float(entry2_count) / float(entry1_count)
        self.assertTrue(ratio > 2.0)
        self.assertTrue(ratio < 4.0)

    def test_table_get_entry_degenerate(self):
        """
        Simple case where only one is active.
        """
        TableEntry.objects.filter(pk=self.entry2.pk).update(enabled=False)
        # get a fresh copy of table from DB to avoid cached property
        table = Table.objects.get(pk=self.table.pk)
        self.assertEqual(table.get_random_entry(),
                         self.entry1)

    def test_table_weights_base_case(self):
        """
        Test storing the min/max for each entry.
        """
        self.assertEqual(self.entry1.weight_floor, 1)
        self.assertEqual(self.entry1.weight_ceiling, 25)
        self.assertEqual(self.entry2.weight_floor, 26)
        self.assertEqual(self.entry2.weight_ceiling, 100)

    def test_get_table_by_path_good(self):
        self.assertEqual(get_table_by_dotted_name('parent'),
                         self.parent_table)
        self.assertEqual(get_table_by_dotted_name('parent.child'),
                         self.child_table)
        self.assertEqual(get_table_by_dotted_name('parent.child.grandchild'),
                         self.grandchild_table)

    def test_get_table_by_path_bad_top_level(self):
        with self.assertRaises(TableNotFound) as ex:
            get_table_by_dotted_name('bad_name')
        self.assertTrue('bad_name' in str(ex.exception))

    def test_get_table_by_path_bad_nested_level(self):
        with self.assertRaises(TableNotFound) as ex:
            get_table_by_dotted_name('parent.bad_child')
        self.assertTrue('parent.bad_child' in str(ex.exception))

    def test_prevent_duplicate_top_level_tables(self):
        """
        Don't allow the creation of two tables with same name and no parents.
        """
        Table.objects.create(name='duplicate', parent=None)
        with self.assertRaises(IntegrityError):
            Table.objects.create(name='duplicate', parent=None)

    def test_duplicate_table_names_with_same_parent(self):
        Table.objects.create(name='duplicate', parent=self.table)
        with self.assertRaises(IntegrityError):
            Table.objects.create(name='duplicate', parent=self.table)

    def test_duplicate_table_names_with_different_parents(self):
        Table.objects.create(name='duplicate', parent=self.table)
        Table.objects.create(name='duplicate', parent=None)
        Table.objects.create(name='duplicate', parent=self.child_table)
