# urls.py
from django.urls import path, include
from tables.api import TableResource, TableEntryResource
from tastypie.api import NamespacedApi, Api


# api = NamespacedApi(api_name='v1', urlconf_namespace='V1')
api = Api()
api.register(TableEntryResource())
api.register(TableResource())

urlpatterns = [
    path("", include(api.urls)),
]
