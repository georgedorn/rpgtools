from django.conf import settings
from django.db.models import signals
from django.utils.text import get_text_list
from django.db import IntegrityError
from django.core.exceptions import FieldDoesNotExist
from django.utils.translation import gettext as _


def check_unique_together(sender, **kwargs):
    """
    Check models unique_together manually. Django enforced unique together only the database level, but
    some databases (e.g. SQLite) doesn't support this.

    usage:
        from django.db.models import signals
        signals.pre_save.connect(check_unique_together, sender=MyModelClass)

    or use auto_add_check_unique_together(), see below
    """
    instance = kwargs["instance"]
    for field_names in sender._meta.unique_together:
        model_kwargs = {}
        for field_name in field_names:
            try:
                data = getattr(instance, field_name)
            except FieldDoesNotExist:
                # e.g.: a missing field, which is however necessary.
                # The real exception on model creation should be raised.
                continue
            model_kwargs[field_name] = data

        query_set = sender.objects.filter(**model_kwargs)
        if instance.pk != None:
            # Exclude the instance if it was saved in the past
            query_set = query_set.exclude(pk=instance.pk)

        count = query_set.count()
        if count > 0:
            field_names = get_text_list(field_names, _("and"))
            msg = _("%(model_name)s with this %(field_names)s already exists.") % {
                "model_name": instance.__class__.__name__,
                "field_names": field_names,
            }
            raise IntegrityError(msg)


def auto_add_check_unique_together(model_class):
    """
    Add only the signal handler check_unique_together, if a database without UNIQUE support is used.
    """
    for database_name, database_settings in settings.DATABASES.items():
        if "sqlite3" in database_settings["ENGINE"]:
            signals.pre_save.connect(check_unique_together, sender=model_class)
