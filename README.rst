========
rpgtools
========

A collection of tools for running and playing RPGs.

Requirements
============

Built for python 3.6 and django 2.0.  Might work on other versions, with effort.
TODO: Deployment to a web server?


Installation
============

This assumes a system with python 3 and virtualenv already installed::

  git clone https://gitlab.com/georgedorn/rpgtools.git # or your own fork
  cd rpgtools
  virtualenv --python=`which python3` .virt  # creates virtualenv for python3 in ./rpgtools/.virt  or use your own prefs
  source .virt/bin/activate
  which pip  # should be /.../rpgtools/.virt/bin/pip
  pip install -r requirements.txt
  
  # to run tests:
  ./manage.py test
  
  # to run tests and get coverage report
  coverage run ./manage.py test
  coverage html --omit=".virt/*"  # then open rpgtools/htmlcov/index.html in a browser

  # if you intend to use this, you'll need to create the database and populate it with creatures and PCs
  ./manage.py migrate  # creates sqlite3 db by default
  ./manage.py game_shell  # starts the game shell to create creatures, PCs, NPCs, and run combats; see below

  # to run local web instance
  ./manage.py runserver 0.0.0.0:8000  # ip optional if only accessing via localhost
  # then browse to 127.0.0.1:8000/combat/  which will generate an error if there's no combat yet

Features
========

Tables
------

A lightweight random table database, using many features from TableSmith:

* Open-ended tables, allowing adding more entries without rewriting the probabilities.
* Inter-table references, allowing one table to return results containing nested dice rolls (e.g. a random encounter with "13 orcs" instead of "3d6 orcs") or even nested entries from other tables (e.g. "1 hill giant armed with a greatclub" instead of "1 hill giant armed with [random giant weapon]".

Initiative Tracker
------------------

* Quickly create a combat and display turn order, allowing on-the-fly order changes and adding/removing participants.
* Public view of the combat, suitable for displaying on a TV or projector, showing turn order and non-hidden arbitrary information about each particpant.
* Private view of the combat, showing turn order and DM-only notes (damage accumulation, status effects)

Character Database
------------------

* A lazy character tracker, allowing just-in-time prompting for missing data.
* Level-up flag indicating potentially stale data

Player View
-----------

* Phone-friendly url for each character
* Shows public data (stats/skills)
* Shows initiative position in current combat, if any
* Web notifications for upcoming turns (configurable?)

Future ideas:

* Spellbooks and currently-prepared spells, with spell details from the SRD.

Command line REPL
-----------------

DM-facing interface for:

* creating and managing combats, characters, monsters
* rolling group skill checks (everybody's Spot or Listen) with just-in-time prompting for missing skills/stats
* adding arbitrary private notes on practically any object - characters, monsters, world state


Usage Example
-------------
`./manage.py game_shell` starts a CLI for interacting with your PCs, NPCs, monsters, and run combats.
On first run, it will create a default Party and Combat object, available in the shell as 'party' and 'combat'.


Creating Characters
...................

`>> bob = create_character('Bob')`

Bob now appears in the party::

  >> party.pcs.Bob
  Bob
  ---
  
  >>

Not very useful without stats.  We can set those now or lazily when they are needed::

  >> party.pcs.Bob.set_stat('Knowledge (Nature):', 6)

  >> party.pcs.Bob.roll('Move Silently')  # Bob doesn't have this yet, so the system asks for it
  Bob's Move Silently: 0
  10
  
  >> party.roll('Hide')  # Roll 'Hide' for everybody in the party.  It knows other PCs' Hide skills, but asks for Bob's:
  Bob's Hide: 1
  Templeton:    38
  Ismelda:  19
  Bob:  18

  >> party.pcs.Bob  # show Bob's stat block
  
  Bob
  ---
  Hide: 1
  Knowledge (Nature):: 6
  Move Silently: 0

Creating Monsters
.................

There are multiply ways to create new monsters:
`>> goblin = create_monster('goblin')` creates a new monster directly in the monster library.

`>> combat.add_monster('goblin')` adds a monster to the current combat, creating it on-the-fly if it isn't already in the library.

Running Combat
..............

The shell automatically loads the last combat (or creates a new one) as `combat`.

`>> combat.add(party)` adds everybody in the party to a combat.  This really only works for new combats.

`>> combat.add(party.pcs.Bob)` adds just Bob to the combat.

`>> combat.remove('Bob')` takes Bob out of the combat.  TODO: this API isn't great, see issue #18

`>> combat.start()` starts the combat, prompting for the initiative stat for any creatures that don't already have it saved

Levelling Up
............

When a character levels up, you can mark their stats as obsolete without deleting them outright:

`>> party.pcs.Bob.level_up()` marks Bob's stats as old; the shell will ask you for new ones as they are used.

Design Transcript
=================
A hypothetical play example; this no longer matches how the shell works, but presents early ideas::

    >>> players = get_active_pcs()
    >>> c = Combat(players)
    >>> c.add_monster('orc')
    ? Initiative stat for "orc": 0
    
    >>> c.add_monsters("2d4", "goblin")
    ? Initiative stat for "goblin": 1
    Added 4 goblins.
    
    >>> c.roll_surprise(players)  # "all" is default
    ? Spot stat for "Bob the Paladin": 3
    
    "Bob the Paladin" is surprised
    "Fizzbin" is surprised
    
    >>> c.start()
    Current turn:  goblin #1  (init 21)
    On deck: Bob The Paladin (init 20)  [protection from evil, surprised, flat-footed]
    
    >>> c.show()
    goblin #1  (init 21)
    Bob The Paladin (init 20)  [protection from evil, surprised, flat-footed]
    goblin #3 (init 19) [flat-footed]
    Slick (init 18) [uncanny dodge, flat-footed]
    goblin #2 (init 17) [flat-footed]
    Fizzbin (init 12) [surprised, flat-footed]
    goblin #4 (init 10) [flat-footed]
    
    >>> c.next()
    Current turn: Bob The Paladin (init 20)  [protection from evil, surprised]
    On deck: goblin #3 (init 19) [flat-footed]
    
    >>> c.next()
    Current turn:  goblin #3  (init 19)
    On deck: Slick (init 18) [uncanny dodge, flat-footed]
    
    >>> c.next()
    Current turn:  Slick (init 18) [uncanny dodge]
    On deck: goblin #2
    
    >>> c.kill('goblin')
    ? Which goblin?  2
    Goblin #2 was removed from combat.

    >>> c.add_effect('goblin #4', 'stunned')
    Goblin #4 is now: stunned, flat-footed
    
    >>> c.show()
    Slick (init 18) [uncanny dodge, flat-footed]
    Fizzbin (init 12) [surprised, flat-footed]
    goblin #4 (init 10) [stunned, flat-footed]
    goblin #1  (init 21)
    Bob The Paladin (init 20)  [protection from evil, surprised, flat-footed]
    goblin #3 (init 19)


MVP
===

Features needed for barely-working app to start iterating on:

* Minimal character DB, allowing abitrary skills and stats to be added without a character sheet schema.
* Minimal monster DB, allowing on-the-fly creation of monsters.
* CLI for rolling group initiatives and group skill rolls.
* Player web view with on-deck notifications.
* Public web view of current initiative, for use with a wall-mounted TV.
